# Words Counter
  - Get 100 most common words in a given webpage.
  - Collect the list of all words ever appeared in the search.

Developed by [Dmitry Kvochkin][dk]

### Tech
Technologies used to build WordsCounter:

* [ReactJs] - JavaScript library for building user interfaces
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework
* [Puppeteer] - High-level API to control Chrome /Chromium over the DevTools Protocol.
* [MongoDB] - Free and open-source cross-platform document-oriented database program.
* [Docker] - Docker containers 
* [Docker Compose] - Tool for defining and running multi-container Docker applications



### Running the Application

Make sure you have [Docker] and [Docker Compose] installed.

```sh
$ cd words-counter
$ docker-compose up
```

Open a browser and navigate to

```sh
http://localhost:5000/
```

Try out:
```
https://docs.docker.com/compose/startup-order/
```

To login use:
```
userame: admin
password: admin
```

### Development

For development purposes you might want to run the application locally. (You will have to run a MongoDb instance).
Open your favorite Terminal and run these commands.
(from root folder)
First Tab - run the api:
```sh
$ cd server
$ npm install
$ npm run watch
```
Second Tab - run the client:
```sh
$ cd client
$ npm install
$ npm start
```

Open the browser and navigate to:
```
http://localhost:3000/
```

### Todos

 - Create signed token for Auth & Integrate authentication with DB
 - Write Tests
 - Set up environment variables
 - Add proper logging (e.g. bunyan)
 - Exclude prepositions and articles (code wrapper already created)
 - Code refactoring
 

   [dk]: <http://dmitrykvochkin.com>
   [Puppeteer]: <https://github.com/GoogleChrome/puppeteer>
   [node.js]: <http://nodejs.org>
   [express]: <http://expressjs.com>
   [ReactJS]: <https://reactjs.org/>
   [Docker]: <https://www.docker.com/>
   [Docker Compose]: <https://docs.docker.com/compose/>
   [MongoDB]: <https://docs.mongodb.com/>
