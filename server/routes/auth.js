const express = require('express');

const router = express.Router();

/* GET authenticate username+password. */
router.post('/', (req, res) => {
  // for demonstration purposes and speed of development only
  // this validation should be done against database (with encrypted passwords) 
  res.send({ token: 'xxx' });
});

module.exports = router;
