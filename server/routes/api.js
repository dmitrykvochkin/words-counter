const express = require('express');
const router = express.Router();
const counter = require('./../lib/counter');
const db = require('./../lib/db');


/* 
  #TODO: Finish auth middleware.
  make sure to verify existence and validity of JWT token in request headers
  we should have a secret that signs jwt token on authentication
*/
const ensureAuthorized = (req, res, next) => {
  if(!req.headers.authorization){
    return res.status(401).send();
  }
  next();
}


/* POST count number of words in a page  */
router.post('/search', async (req, res) => {
  const url = req.body.url;
  
  try{
    const { top100, all } = await counter.countWords(url);

    // save all words to db
    // make it async - this way user doesn't get blocked
    db.saveWords(all);

    res.send({
      url: url,
      words: top100  
    });
  }catch(err){
    res.status(500).send({error: err.message});
  }

});



/* GET all time words county */
router.get('/search/history', ensureAuthorized, async (req, res) => {
  
  const getWords = db.getWords({
    page: req.query.page
  });
  const getWordsCount = db.getWordsCount();

  const result = await Promise.all([getWordsCount, getWords]);

  res.send({
    wordsCount: result[0],
    words: result[1]
  });
});


module.exports = router;
