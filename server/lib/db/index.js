'use strict';
const db = require('./mongoUtil.js');
const PAGE_LIMIT = 50;

const saveWords = async (words) => {
    const col = db.getDb().collection('words');

    const batch = col.initializeUnorderedBulkOp();

    words.forEach(word => {
        batch.find({
            'value': word.value
        }).upsert().updateOne({
            $inc: {'count': word.count}
        });
    });

    try{
        const res = await batch.execute();
        return res;
    }catch(err){
        return new Error('Could not update words count in DB.')
    }
};


const getWords = async (params) => {
    const col = db.getDb().collection('words');

    const skip = (params.page || 0) * PAGE_LIMIT;
    const res = await col.find({}).sort({count: -1}).skip(skip).limit(PAGE_LIMIT).toArray();

    return res;
}

const getWordsCount = async () => {
    const col = db.getDb().collection('words');

    const res = await col.count();

    return res;
}

module.exports = {
    saveWords,
    getWords,
    getWordsCount,
}