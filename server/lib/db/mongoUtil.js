'use strict';

const MongoClient = require('mongodb').MongoClient;
// Connection url #TODO: get from ENV
const url = 'mongodb://localhost:27017/WordCounter';
let client, db, retryCount = 0;
const MAX_RETRY = 5;
const RETRY_TIMEOUT = 1000*3;


const connect = async () => {
    // Connect using MongoClient
    MongoClient.connect(url, function(err, c) {
        if(err){
            if(++retryCount > MAX_RETRY){
                throw err;
            }
            console.log(`Failed to connect to DB. retrying in ${RETRY_TIMEOUT/1000}s.`);
            setTimeout(connect, RETRY_TIMEOUT);
            return;
        }
        console.log('Connected to DB!')
        client = c;
        db = c.db();
    });

}


const getDb = () => {
    return db;
}

module.exports = {
    connect,
    getDb,
}
