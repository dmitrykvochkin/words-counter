const mongoUtil = require('./mongoUtil.js');
const db = require('./index.js');

mongoUtil.connect();

setTimeout(() => {
    db.saveWords([
        {
          value: 'hello',
          count: 10
        },
        {
          value: 'friend',
          count: 23
        },
        {
          value: 'party',
          count: 12
        }
      ]
    );
}, 1000*3);