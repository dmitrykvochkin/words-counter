/*
  use this file pattern to quickly call/develop components of your application
*/
const counter = require('./index.js');

const url = 'https://stackoverflow.com/questions/298750/how-do-i-select-text-nodes-with-jquery';

(async () => {
    try{
        const { top100, all } = await counter.countWords(url);
        console.log('result: ', top100);
      }catch(err){
        console.log('err', err);
      }
})();
