'use strict';
const puppeteer = require('puppeteer');

const TOP_N = 100;
const WORDS_TO_SKIP = [''];


const isNumeric = (num) => {
    return !isNaN(num)
  }


/**
 * 
 * @param {*} word 
 */
const shouldSkipWord = (word) => {
    if(
        WORDS_TO_SKIP.indexOf(word) >= 0
        || isNumeric(word)
    ){
        return true;
    }
    //#TODO: add exclusions for prepositions & articles

    return false;
};


/**
 * 
 * @param {*} words 
 */
const groupWords = (words) => {
    let allWords = {};
    
    words.forEach(word => {
        const w = word.toLowerCase();
        if(allWords[w]){
            allWords[w].count++;
            return;
        }

        if(shouldSkipWord(w)){
            return;
        }

        allWords[w] = {
            value: w,
            count: 1
        };
    });

    return allWords;
}


const getTop100 = (groupedWords) => {
    const arr = Object.values(groupedWords);
    arr.sort( (a, b) => {
        return b.count - a.count;
    });

    return arr.slice(0, TOP_N);
}


const countWords = async (url) => {
    try{
        const browser = await puppeteer.launch({ 
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox'
            ]
        });
        const page = await browser.newPage();

        await page.goto(url, {waitUntil: 'networkidle2'});

        // we use jquery to count words
        await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'});

        const words = await page.evaluate(async () => {
            // get webpage nodes
            // iterate over nodes
            // if node type is text(3) -> get words as Array
            const words = $('body  *').contents().map(function () {
                if (this.nodeType == 3 && this.nodeValue.trim() != "") //check for nodetype text and ignore empty text nodes
                    return this.nodeValue.trim().split(/\W+/);  //split the nodevalue to get words.
            }).get();

            return words;
        });        

        // close browser in order to "release" resources
        await browser.close();
        
        // count individual words
        const groupedWords = groupWords(words);

        return { 
            top100: getTop100(groupedWords),
            all: Object.values(groupedWords)
        };
    }catch(err){
        // #TODO: if we fail to count words using headless browser (some websites block headless scrapers)
        //        fallback to traditional fetch method
        throw err;
    }    
    
}


module.exports = {
    countWords,
};
  