const LOCAL_STORAGE_KEY = 'jwt';
const AUTH_API = 'http://localhost:3001/auth'; //#TODO: get from ENV variable 

const Auth = {
    token: () => localStorage.getItem(LOCAL_STORAGE_KEY),
    isAuthenticated: () => {
            const token = localStorage.getItem(LOCAL_STORAGE_KEY);
            // #TODO: verify token expiation date
            return token ? true : false;
    },    
    authenticate({username, password}) {
        return new Promise((resolve, reject) => {
            fetch(AUTH_API, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },          
                body: JSON.stringify({
                    username, 
                    password
                })
            }).then((response) => {
                return response.json();
              })
              .then((res) => {
                this.setToken(res.token)
                resolve();
              }, reject);           
        });
    },
    setToken(token) { 
            localStorage.setItem(LOCAL_STORAGE_KEY, token);
    },        
    removeToken() { 
            localStorage.removeItem(LOCAL_STORAGE_KEY); 
    },
    logout() {
        return new Promise((resolve, reject)=>{
            this.removeToken();
            resolve();
        });
    }
  };
  
  export default Auth;