import Auth from './Auth';

const API_URL = 'http://localhost:3001/api'; //#TODO: get from ENV variable

const Api = {
    'getWordsCount': (url) => 
        fetch(`${API_URL}/search`, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },          
            body: JSON.stringify({
                url
            })
        }).then(response => response.json()),
    
    'getStats': (page) => 
        fetch(
            `${API_URL}/search/history?page=${page}`, 
            {
                headers: {
                    'Authorization': 'Bearer ' + Auth.token(),
                }
            }
        ).then(response => response.json())
};
  
  export default Api;