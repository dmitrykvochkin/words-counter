import React from 'react';
import { TagCloud } from "react-tagcloud";
import { Link } from "react-router-dom";
import './SearchResults.css';


const SearchResults = (props) => {
    const { error, search } = props;
    
    // handle errors first
    if(error){
      return (
        <div>
          <p>
            Could not count words for: {search.url}
          </p>
          <p>
            Error: {error}
          </p>
        </div>
      );
    }
    
    return (
      <div className="SearchResults"> 
        <div>
          Words count for <b>{search.url}</b>
        </div>
        <TagCloud minSize={12}
          maxSize={35}
          tags={search.words}
          colorOptions={{ hue: '#333', luminosity: 'dark' }}
        />
        <Link to="/" className="SearchResults__next_search">
            Perform another search
        </Link>
      </div>
    )
}
  

export default SearchResults;