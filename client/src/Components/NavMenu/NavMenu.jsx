import React from 'react';
import {
    Link,
    withRouter
} from "react-router-dom";
import Button from '@material-ui/core/Button';
import './NavMenu.css';
import Auth from './../../Services/Auth';

class NavMenu extends React.Component {
    constructor(args) {
      super(args);
  
      this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout(){
        Auth.logout()
            .then(()=>{
                this.props.history.push("/");
            }, (error) => {
                console.error(error);
            });
    }

    render(){
        return (
            <div className="NavMenu">
                <Link to="/">Home</Link>
                <div>
                    {
                        Auth.isAuthenticated() ?                         
                        <div>
                            <Link to="/stats">Stats</Link>
                            <Button onClick={this.handleLogout}>
                                Logout
                            </Button>                            
                        </div>
                        :
                        <Link to="/login">Login</Link>
                    }
                </div>            
            </div>
        )
    }
}

export default withRouter(NavMenu);