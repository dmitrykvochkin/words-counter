import React from 'react';
import {
    withRouter
} from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import './SearchBar.css';

class SearchBar extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        url: ''
      };

      this.handleSearch = this.handleSearch.bind(this);
    }

    handleSearch(e){
        this.props.history.push({
            pathname: '/search',
            search: `?url=${this.state.url}`,
          });
    }

    handleChange(name, event){
        this.setState({
          [name]: event.target.value,
        });
      }

    render(){
        return (
            <form onSubmit={this.handleSearch} autoComplete="off" className="SearchBar">
                <TextField
                    id="outlined-bare"
                    placeholder="http://example.com/path/to/page"
                    className="SearchBar__input"
                    margin="normal"
                    variant="outlined"
                    value={this.state.url}
                    onChange={(e) => this.handleChange('url', e)}
                />
                <Button 
                    className="SearchBar__submit"
                    type="submit"
                    variant="contained"                    
                >
                    Search
                </Button>
            </form>
        )
    }
}

export default withRouter(SearchBar);