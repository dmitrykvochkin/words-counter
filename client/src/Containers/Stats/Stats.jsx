import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './Stats.css';
import Api from './../../Services/Api';

class Stats extends React.Component {
  constructor(args) {
    super(args);
    
    this.state = {
      isLoading: true,
      error: null,
      page: 0,
      wordsCount: 0,
      words: [],        
    }

    this.loadStats = this.loadStats.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
  }

  componentDidMount() {
    this.loadStats();
  }

  handleChangePage(event, page){
    this.setState({
      page: page
    }, () => {
      this.loadStats();
    });
  }

  loadStats() {
    this.setState({
      isLoading: true,
    });

    Api.getStats(this.state.page).then((response) => {
      this.setState({
        isLoading: false,
        error: response.error,
        wordsCount: response.wordsCount,
        words: response.words
      });
    }, (error) => {
      console.error('error:', error);      
      this.setState({
        isLoading: false,
        error: 'Could not fetch results...'
      });
    });
  }

  render() {
    const { isLoading, error, page, wordsCount, words } = this.state;

    if (isLoading) {
      return (
        <div>
          Loading...
        </div>
      );
    }

    if (error) {
      return (
        <div>
          { error }
        </div>
      );
    }

    return(
      <div>
      <Paper className="Stats">
        <div className="Stats__Table_wrapper">
          <Table className="Stats__Table">
            <TableBody>
              {words.map(row => {
                return (
                  <TableRow key={row.value}>
                    <TableCell component="th" scope="row">
                      {row.value}
                    </TableCell>
                    <TableCell numeric>{row.count}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  colSpan={3}
                  count={wordsCount}
                  rowsPerPage={50}
                  rowsPerPageOptions={[50]}
                  page={page}
                  onChangePage={this.handleChangePage}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </Paper>
      </div>
    )
  }
}

export default Stats;
