import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import './Login.css';
import Auth from './../../Services/Auth';

class Login extends React.Component {
  constructor(args) {
    super(args);
    
    this.state = {
      error: null,
      username: 'admin',
      password: 'admin',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin(){
    Auth.authenticate(this.state)
      .then(() => {
        this.props.history.push("/stats");
      }, (error) => {
        this.setState({
          error: error.message,
        })
      });
  }

  handleChange(name, event){
    this.setState({
      [name]: event.target.value,
    });
  }

  render() {
    return(
      <div className="Login">
        <p className="Login__error">
          {this.state.error}
        </p>
        <TextField
          id="standard-name"
          label="Username"
          className="Login__input"
          value={this.state.username}
          onChange={(e) => this.handleChange('username', e)}
          margin="normal"
        />
        <TextField
          id="standard-password-input"
          label="Password"
          className="Login__input"
          type="password"
          autoComplete="current-password"
          onChange={(e) => this.handleChange('password', e)}
          margin="normal"
        />
        <Button 
          variant="contained"
          onClick={this.handleLogin}
        >
          Login
        </Button>
      </div>
    )
  }
}

export default Login;
