import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import './App.css';
import PrivateRoute from './../../Components/PrivateRoute';
import Search from './../Search';
import Stats from './../Stats';
import Home from './../Home';
import NavMenu from './../../Components/NavMenu'; 
import Login from './../Login';

class App extends Component {
  render() {
    return (
      <Router>
      <div>
        <NavMenu />
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/search" component={Search} />
        <PrivateRoute path="/stats" component={Stats} />
      </div>
    </Router>
    );
  }
}

export default App;
