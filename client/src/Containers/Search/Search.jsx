import React from 'react';
import qs from 'query-string';
import Api from './../../Services/Api';
import SearchResults from './../../Components/SearchResults';


class Search extends React.Component {
  constructor(args) {
    super(args);

    this.state = {
      search: {
        url: '',
        words: null
      },
      isLoading: true,
      error: null,
    };
  }

  componentDidMount() {
    const { url } = qs.parse(this.props.location.search);
    if(!url) {
      this.props.history.push("/");
      return;
    }
    Api.getWordsCount(url).then((response) => {
      this.setState({
        isLoading: false,
        error: response.error,
        search: { 
          url: url,
          words: response.words
        }
      });
    }, (error) => {
      console.error('error:', error);      
      this.setState({
        isLoading: false,
        error: 'Could not fetch results...',
        search: { 
          url: url,
          words: []
        }
      });
    });
  }

  render() {
    const { search, isLoading, error } = this.state;

    if (isLoading) {
      return (
        <div style={{textAlign: 'center'}}>
          Loading...
        </div>
      );
    }

    return (
      <SearchResults error={error} search={search} />
    )

  }
}

export default Search;
