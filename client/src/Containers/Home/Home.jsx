import React from 'react';
import './Home.css';
import SearchBar from './../../Components/SearchBar';
class Home extends React.Component {
  render() {
    return(
      <div className="Home">
        <SearchBar />
      </div>
    )
  }
}

export default Home;
